# Example Meteor Blog App

This is a very simple example app that uses the [`ryw:blog`](https://github.com/Differential/meteor-blog) package along with bootstrap and iron router. 

You can view it live at [blog-example.meteor.com](http://blog-example.meteor.com)

#todo


recognize users upon login and load their settings.

add splash page

add blog access only via admin and priv. users..

provide cms link for regular users.

automate data import for site analytics


